%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Medium Length Professional CV - RESUME CLASS FILE
%
% This template has been downloaded from:
% http://www.LaTeXTemplates.com
%
% This class file defines the structure and design of the template.
%
% Original header:
% Copyright (C) 2010 by Trey Hunner
%
% Copying and distribution of this file, with or without modification,
% are permitted in any medium without royalty provided the copyright
% notice and this notice are preserved. This file is offered as-is,
% without any warranty.
%
% Created by Trey Hunner and modified by www.LaTeXTemplates.com
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\ProvidesClass{resume}[2010/07/10 v0.9 Resume class]

\LoadClass[11pt]{article} % Font size and paper type

\usepackage[parfill]{parskip} % Remove paragraph indentation
\usepackage{array} % Required for boldface (\bf and \bfseries) tabular columns
\usepackage{ifthen} % Required for ifthenelse statements
%\newfontfamily\titlefont{DejaVu Serif}

%Colour Section-------------------------------------
\usepackage[dvipsnames]{xcolor}
\colorlet{bluegray}{NavyBlue!90!Black!70!}
\colorlet{bluegray2}{NavyBlue!70!Black!60}
\colorlet{bluegray3}{NavyBlue!60!Gray!100}
\definecolor{uu-red}{RGB}{153,0,0}
\colorlet{uu-red-light}{uu-red!60!}
\colorlet{uu-red-light2}{uu-red!80!Black!40!}
\colorlet{newgray}{black!75!}
\definecolor{offwhite}{RGB}{237,234,224}
%\definecolor{offwhite2}{RGB}{227,218,201}

%\usepackage[dvipsnames]{xcolor}
%\pagecolor{offwhite!10!}
%Page border-------------------------
\usepackage{fancyhdr}
\pagestyle{fancy}
\renewcommand{\headrulewidth}{0pt}
%\fancyhf{}
\usepackage{tikz}
\usepackage{tikzpagenodes}
\newcommand{\bottomline}{\tikz[remember picture,overlay]
    \draw[uu-red-light,line width=2mm]
    (current page.south west)
    --
    (current page.south east)
    ;}
\newcommand{\topline}{\tikz[remember picture,overlay]
    \draw[uu-red-light,line width=2mm]
    (current page.north west)
    --
    (current page.north east)
    ;}

\cfoot[\bottomline]{\bottomline}
\chead[\topline]{\topline}

%------------------------------------
%---------------------------------------------------

\usepackage{fontawesome5} %For cool icons

%\pagestyle{empty} % Suppress page numbers

%----------------------------------------------------------------------------------------
%	HEADINGS COMMANDS: Commands for printing name and address
%----------------------------------------------------------------------------------------

\def \name#1{\def\@name{#1}} % Defines the \name command to set name
\def \@name {} % Sets \@name to empty by default

\def \addressSep {$\diamond$} % Set default address separator to a diamond

% One, two or three address lines can be specified
\let \@addressone \relax
\let \@addresstwo \relax
\let \@addressthree \relax

% \address command can be used to set the first, second, and third address (last 2 optional)
\def \address #1{
  \@ifundefined{@addressone} { %Address
    \def \@addressone {\color{uu-red-light}\faIcon{home} #1}
  }{
   \@ifundefined{@addresstwo}{ %telephone
     \def \@addresstwo {\color{uu-red-light}\faIcon{phone-square-alt} #1}
  }{
   \@ifundefined{@addressthree}{ %email
     \def \@addressthree {\color{uu-red-light}\faIcon{at} #1}
  }{}}}}


% \printaddress is used to style an address line (given as input)
\def \printaddress #1{
  \begingroup
    \def \\ {\addressSep }
    \centerline{#1}
  \endgroup
  \par
  \addressskip
}

% \printname is used to print the name as a page header
\def \printname {
  \begingroup
    \hfil{\MakeUppercase{\namesize\titlefont \@name}}\hfil
    \nameskip\break
  \endgroup
}

\def \datestyle #1{ %Note: Requires fontspec to load a font called 'datefont' for this to work
   \begingroup
      \hfill{\color{newgray}\datefont #1}
   \endgroup
}

%----------------------------------------------------------------------------------------
%	PRINT THE HEADING LINES
%----------------------------------------------------------------------------------------

\let\ori@document=\document
\renewcommand{\document}{
  \ori@document  % Begin document
  \printname % Print the name specified with \name
  \@ifundefined{@addressthree}{
    \printaddress{\@addressone \hspace{1ex}|\hspace{1ex} \@addresstwo}
  }{
   \printaddress{\@addressone \hspace{1ex}|\hspace{1ex} \@addresstwo \hspace{1ex}|\hspace{1ex} \@addressthree}
   }
   \@ifundefined{@addresstwo}{
     \printaddress{\@addressone}
   }{}

}

%----------------------------------------------------------------------------------------
%	SECTION FORMATTING
%----------------------------------------------------------------------------------------

% Defines the rSection environment for the large sections within the CV
\newenvironment{rSection}[1]{ % 1 input argument - section name
  \sectionskip
  {\color{Black} \MakeUppercase{\large\titlefont #1}} % Section title
  \sectionlineskip
  {\color{uu-red-light2}\hrule height 0.4mm} % Horizontal line
  \begin{list}{}{ % List for each individual item in the section
    \setlength{\leftmargin}{1.5em} % Margin within the section
  }
  \item[]
}{
  \end{list}
}

%----------------------------------------------------------------------------------------
%	WORK EXPERIENCE FORMATTING
%----------------------------------------------------------------------------------------

\newenvironment{rSubsection}[4]{ % 4 input arguments - company name, year(s) employed, job title and location
 {\bf #1} \hfill {#2} % Bold company name and date on the right
 \ifthenelse{\equal{#3}{}}{}{ % If the third argument is not specified, don't print the job title and location line
  \\
  {\em #3} \hfill {\em #4} % Italic job title and location
  }\smallskip
  \begin{list}{$\cdot$}{\leftmargin=0em} % \cdot used for bullets, no indentation
   \itemsep -0.5em \vspace{-0.5em} % Compress items in list together for aesthetics
  }{
  \end{list}
  \vspace{0.5em} % Some space after the list of bullet points
}

% The below commands define the whitespace after certain things in the document - they can be \smallskip, \medskip or \bigskip
\def\namesize{\huge} % Size of the name at the top of the document
\def\addressskip{\smallskip} % The space between the two address (or phone/email) lines
\def\sectionlineskip{\medskip} % The space above the horizontal line for each section
\def\nameskip{\bigskip} % The space after your name at the top
\def\sectionskip{\medskip} % The space after the heading section
